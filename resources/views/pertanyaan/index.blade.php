@extends('master')

@section('titles', 'Sanbercode | Pertanyaan')

@section('content')
<a href="/pertanyaan/create" class="btn btn-success btn-sm">Buat Pertanyaan</a>

<table class="table table-bordered">
    <tr>
        <th>Judul</th>
        <th>Isi</th>
        <th>Aksi</th>
    </tr>
@foreach ($data as $v)
    <tr>
        <td><b>{{$v->judul}}</b></td>
        <td>{{$v->isi}}</td>
        <td>
            <a href="/pertanyaan/{{$v->id}}" class="btn btn-sm btn-primary">Lihat</a>
            <a href="/pertanyaan/{{$v->id}}/edit" class="btn btn-sm btn-warning">Update</a>
            <form action="{{ url('/pertanyaan', ['id' => $v->id]) }}" method="post">
                <input class="btn btn-sm btn-danger" type="submit" value="Hapus" />
                @method('delete')
                @csrf
            </form>
        </td>
    </tr>
@endforeach

</table>
@endsection