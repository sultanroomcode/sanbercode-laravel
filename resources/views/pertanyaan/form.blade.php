@extends('master')

@section('titles', 'Sanbercode | '.($isNewRecord?'Buat':'Update').' Pertanyaan')

@section('content')

@if (!$isNewRecord)
<form action="/pertanyaan/{{ $data->id}}" method="post">
@else
<form action="/pertanyaan" method="post">
@endif

    @csrf
    @if (!$isNewRecord)
    @method('PUT')
    @endif
    Judul : <br>    
    <input type="text" class="form-control" name="judul" id="judul" value="{{ $data->judul?? ''}}">

    Isi : <br>    
    <textarea class="form-control" name="isi" id="isi">{{ $data->isi ?? ''}}</textarea>

    <input type="submit" value="Simpan" class="btn btn-sm btn-success">
</form>
@endsection