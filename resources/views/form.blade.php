@extends('master')

@section('titles', 'Register | Sanbercode')

@section('content')
<h1>Buat Account Baru!</h1>

<h3>Sign Up Form</h3>

<form action="/welcome" method="post">
    @csrf
    First name:
    <br><br>
    <input type="text" name="first_name" id="first_name">
    <br><br>

    Last name:
    <br><br>
    <input type="text" name="last_name" id="last_name">
    <br><br>

    Gender:
    <br><br>
    <input type="radio" value="laki-laki" name="gender" id="gender"> Male
    <br>
    <input type="radio" value="perempuan" name="gender" id="gender"> Female
    <br>
    <input type="radio" value="lainnya" name="gender" id="gender"> Other
    <br>
    <br>
    Nationality:
    <br>
    <br>
    <select name="nationality" id="nationality">
        <option value="id">Indonesian</option>
        <option value="sg">Singaporean</option>
        <option value="my">Malaysian</option>
        <option value="au">Australian</option>
    </select>
    <br>
    <br>

    Language Spoken:
    <br>
    <br>
    <input type="checkbox" name="lang" id="lang" value="id"> Bahasa Indonesia <br>
    <input type="checkbox" name="lang" id="lang" value="en"> English <br>
    <input type="checkbox" name="lang" id="lang" value="other"> Other <br>
    <br>
    Bio:
    <br>
    <br>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
    <br>
    <input type="submit" value="Sign Up">
</form>

<hr>
&copy; 2020 Agus Sutarom at <a href="mailto://agussutarom@gmail.com">agussutarom@gmail.com</a>
@endsection