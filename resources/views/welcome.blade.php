@extends('master')

@section('titles', 'Welcome | Sanbercode')

@section('content')
<h1>SELAMAT DATANG {{$nama}}!</h1>
<h2>Terima kasih telah bergabung di SanberBook. Social Media kita bersama!</h2>

<hr>
&copy; 2020 Agus Sutarom at <a href="mailto://agussutarom@gmail.com">agussutarom@gmail.com</a>
@endsection