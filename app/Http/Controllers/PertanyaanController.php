<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pertanyaan::all();
        return view('pertanyaan.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $isNewRecord = true;
        return view('pertanyaan.form', compact('isNewRecord'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $p = new Pertanyaan();

        $p->judul = $request->judul;
        $p->isi = $request->isi;
        $p->tanggal_dibuat = $p->tanggal_diperbaharui = date('Y-m-d');
        $p->jawaban_tepat_id = null;
        $p->profile_id = 1;

        $p->save();
        return redirect()->route('pertanyaan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Pertanyaan::findOrFail($id);
        // dd($data);
        return view('pertanyaan.view', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $isNewRecord = false;
        $data = Pertanyaan::findOrFail($id);
        // dd($data);
        return view('pertanyaan.form', compact('isNewRecord', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $p = Pertanyaan::findOrFail($id);

        $p->judul = $request->judul;
        $p->isi = $request->isi;
        $p->tanggal_diperbaharui = date('Y-m-d');
        $p->profile_id = 1;

        $p->save();
        return redirect()->route('pertanyaan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $p = Pertanyaan::findOrFail($id);
        $p->delete();
        return redirect()->route('pertanyaan.index');
    }
}
